--Eric Arends
--Alana O'Mara
--Roman Fischer
--John Larson
--Team 2

create table artist
(
aid int auto_increment,
fname varchar (50) not null,
lname varchar (100),
dob date,
gender varchar(1),
primary key (aid)
)
engine=innodb;

create table band
(
bid int auto_increment,
name varchar (100) not null,
year_formed int(4) not null,
primary key (bid)
)
engine=innodb;

create table in_band
(
aid int,
bid int,
date_in date not null,
date_out date,
foreign key (aid) references artist (aid),
foreign key (bid) references band (bid)
)
engine=innodb;

create table album
(
albumid int auto_increment,
published_year int not null,
title varchar (256) not null,
price decimal (10,2) not null,
format varchar(256) not null,
bid int,
primary key (albumid),
foreign key (bid) references band (bid)
)
engine=innodb;

insert into artist (aid, fname, lname, dob, gender)
values (11,'Ice','Cube','1969-6-15','M'),
(12,'Young','Thug','1991-8-16','M'),
(13,'Kanye','West','1977-6-8','M'),
(14,'Beyonce','Knowles','1981-9-4','F'),
(15, 'Rihanna','','1988-2-20','F'),
(16,'Snoop','Dogg','1971-10-20','M'),
(17,'Jay','Z','1969-12-4','M'),
(18,'Drake','','1986-10-24','M');

insert into band (bid, name, year_formed)
values (101, 'Team 2 at 9AM', 2000),
(102, 'The iRats', 2001),
(103, 'Too Much Data', 2003),
(104, 'Terminal Information', 2006),
(105, 'The Text Wranglers', 2006);

insert into in_band (aid,bid,date_in, date_out)
values(11,102,'2002-4-13','2002-11-28');

insert into in_band (aid,bid,date_in)
values (11,102,'2008-12-12'),
(16,102,'2001-12-23'),
(16,101,'2000-4-4'),
(12,101,'2006-9-18'),
(15,103,'2003-2-13'),
(14,103,'2010-5-23'),
(17,105,'2006-8-26'),
(13,105,'2008-8-24'),
(18,104,'2006-5-22');

insert into album (albumid, published_year, title, price, format, bid)
values(01, 2002, 'Dark Side of the Moon', 15.00, 'CD', 102),
(02, 2012, 'Taylor Allderdice', 25.00, 'CD', 102),
(03, 2013, 'Long Live', 30.00, 'CD', 103),
(04, 2011, 'Frampton Comes Alive!', 25.00, 'CD', 103),
(05, 2016, 'The Life of Pablo', 25.00, 'CD', 103),
(06, 2012, 'Watch the Throne', 25.00, 'CD', 103),
(07, 2007, 'Graduation', 25.00, 'CD', 101),
(08, 2008, '808s & Heartbreak', 25.00, 'CD', 104),
(09, 2013, 'Yeezus', 25.00, 'CD', 104),
(10, 2004, 'Tha Carter', 30.00, 'CD', 105),
(11, 2005, 'Tha Carter II', 30.00, 'CD', 105),
(12, 2008, 'Tha Carter III', 30.00, 'CD', 105),
(13, 2011, 'Tha Carter IV', 30.00, 'CD', 105),
(14, 2009, 'We Are Young Money', 30.00, 'CD', 105);

--Select Statements Part 1
select concat(art.fname," ",art.lname) as fullname
from artist as art, band as b, in_band as i
where b.name = 'Team 2 at 9AM'
and art.aid = i.aid and b.bid = i.bid
and date_format(i.date_in,'%Y') <= 2001 and (date_format(i.date_out,'%Y') >= 2001 or i.date_out is null);

select b.name as band_name, concat(art.fname,' ',art.lname) as fullname
from album as a, artist as art, band as b, in_band as i
where b.bid = i.bid
and art.aid = i.aid
and b.bid = a.bid
and a.title = 'Dark Side of the Moon'
and (date_format(date_in,'%Y') <= a.published_year)
and (date_format(date_out,'%Y') > a.published_year or i.date_out is null);

select distinct concat(art.fname," ",art.lname) as fullname, art.gender, date_format(art.dob, "%b %D %Y") as dob, b.name as band_name
from artist as art, band as b, in_band as i
where art.aid = i.aid
and b.bid = i.bid
and ((art.gender = 'F') or (timestampdiff(year, art.dob, curdate()) > 21));

--Select Statements Part 2
select b.name as band_name
from band as b
where not exists
	(select *
	from artist as art, in_band as i
	where art.aid = i.aid and b.bid = i.bid
	and art.gender = 'F');

select concat(art.fname," ",art.lname) as fullname, timestampdiff(year, art.dob, curdate()) as age, b.name as band_name
from artist as art, band as b
where exists
	(select *
	from in_band as i, album as a
	where art.aid = i.aid
	and b.bid = i.bid
	and b.bid = a.bid
	and art.gender = "F"
	and (select count(albumid) from album)>1);

select concat(art.fname," ",art.lname) as fullname, b.name as band_name, timestampdiff(year, i.date_in, ifnull(i.date_out,curdate())) as years_in_band
from artist as art, band as b, in_band as i
where art.aid = i.aid
and b.bid = i.bid;

--Screen grabs are in other file
--***On the last photo Ice Cube showed up as having 0 years in the band, but that is because he left the band a couuple months after first joining it and didnt reach a whole year**
